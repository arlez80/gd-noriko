"""
	Japanese String Library GD Noriko by arlez80 (Yui Kinomoto)
	UTF8のテキストのみ対応しています。
"""

extends Node

class_name JString

const regex_hiragana:String = "[\u3041-\u3096]"
const regex_katakana:String = "[\u30A1-\u30FA]"
const regex_half_katakana:String = "[\uFF66-\uFF9D]"
const regex_kanji:String = "[\u3400-\u4DBF\u4E00-\u9FFF\uF900-\uFA99]"

"""
	コードポイントcの文字をUTF8文字列に変換
	@param	c	unicode code point
	@return	UTF8文字列
"""
static func asc( c:int ) -> String:
	var buf:Array = []

	if c < 0x80:
		buf.append( c )
	elif c < 0x800:
		buf.append( ( ( c >> 6 ) & 0x1F ) | 0xC0 )
		buf.append( ( c & 0x3F ) | 0x80 )
	elif c < 0x10000:
		buf.append( ( ( c >> 12 ) & 0x0F ) | 0xE0 )
		buf.append( ( ( c >> 6 ) & 0x3F ) | 0x80 )
		buf.append( ( c & 0x3F ) | 0x80 )
	else:
		buf.append( ( ( c >> 18 ) & 0x0F ) | 0xF0 )
		buf.append( ( ( c >> 12 ) & 0x3F ) | 0x80 )
		buf.append( ( ( c >> 6 ) & 0x3F ) | 0x80 )
		buf.append( ( c & 0x3F ) | 0x80 )

	buf.append( 0 )

	return PoolByteArray( buf ).get_string_from_utf8( )

"""
	先頭1文字がひらがなか否かを検査する
	@param	s	文字列
	@return	ひらがなの場合はtrueを返す
"""
static func is_hiragana( s:String ) -> bool:
	var c:int = s.ord_at( 0 )
	return 0x3041 <= c and c <= 0x3096

"""
	先頭1文字がカタカナか否かを検査する
	@param	s	文字列
	@return	カタカナの場合はtrueを返す
"""
static func is_katakana( s:String ) -> bool:
	var c:int = s.ord_at( 0 )
	return 0x30A1 <= c and c <= 0x30FA

"""
	先頭1文字が半角カタカナか否かを検査する
	@param	s	文字列
	@return	半角カタカナの場合はtrueを返す
"""
static func is_half_katakana( s:String ) -> bool:
	var c:int = s.ord_at( 0 )
	return 0xFF66 <= c and c <= 0xFF9D

"""
	先頭1文字が漢字か否かを検査する
	@param	s	文字列
	@return	漢字の場合はtrueを返す
"""
static func is_kanji( s:String ) -> bool:
	var c:int = s.ord_at( 0 )
	return (
		# CJK統合漢字拡張A
		( 0x3400 <= c and c <= 0x4DBF )
		# CJK統合漢字
	or	( 0x4E00 <= c and c <= 0x9FFF )
		# CJK互換漢字
	or	( 0xF900 <= c and c <= 0xFA99 )
	)

"""
	ひらがなをカタカナに変換
	@param	s	文字列
	@return	ひらがなをカタカナに置換した文字列
"""
static func to_katakana( s:String ) -> String:
	var r:String = ""

	for t in s:
		if is_hiragana( t ):
			r += asc( t.ord_at( 0 ) + 0x60 )
		else:
			r += t

	return r

"""
	カタカナをひらがなに変換
	@param	s	文字列
	@return	ひらがなをカタカナに置換した文字列
"""
static func to_hiragana( s:String ) -> String:
	var r:String = ""

	for t in s:
		if is_katakana( t ):
			r += asc( t.ord_at( 0 ) - 0x60 )
		else:
			r += t

	return r
