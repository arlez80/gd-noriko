# "GD Noriko" Japanese String Library for GDScript

## Consts

### JString.regex_hiragana

ひらがな用の正規表現パターンを定義

### JString.regex_katakana

カタカナ用の正規表現パターンを定義

### JString.regex_half_katakana

半角カタカナ用の正規表現パターンを定義

### JString.regex_kanji

漢字用の正規表現パターンを定義

## Functions

### JString.is_hiragana( s:String ) -> bool

先頭1文字がひらがなか否かを検査する

### JString.is_katakana( s:String ) -> bool

先頭1文字がカタカナか否かを検査する

### JString.is_half_katakana( s:String ) -> bool

先頭1文字が半角カタカナか否かを検査する

### JString.is_kanji( s:String ) -> bool

先頭1文字が漢字か否かを検査する

### JString.to_katakana( s:String ) -> String

ひらがなをカタカナに変換

### JString.to_hiragana( s:String ) -> String

カタカナをひらがなに変換

## TODO

* もうちょっとなんか入れる

## License

MIT License

## Author

あるる / きのもと 結衣 @arlez80
